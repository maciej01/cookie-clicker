from __future__ import division
import pygame, sys
from pygame.locals import *

global FPS, WINDOWWIDTH, WINDOWHEIGHT

FPS = 60
WINDOWWIDTH = 1200
WINDOWHEIGHT = 800

WHITE = (255, 255, 255)
BLACK = ( 0,   0,   0 )

class Cookie:
	def __init__(self, image, x=50, y=150):
		self.x = x
		self.y = y
		self.image = image

	def render(self, surface):
		surface.blit(self.image, (self.x, self.y))

	def check_click(self, mousex, mousey):
		if (self.x <= mousex <= self.x+500) and (self.y <= mousey <= self.y+500): 
			return True
		else:
			return False

class Text:
	def __init__(self, text, x, y, size, color, middle=False, end=False):
		self.text = text
		self.x = x
		self.y = y
		self.middle = middle
		self.end = end
		self.size = size
		self.color = color
		self.TextObject = pygame.font.SysFont(None, self.size)

	def render(self, surface):
		if self.middle == True:
			txtObject = self.TextObject.render(self.text, True, self.color)
			surface.blit(txtObject, (self.x - txtObject.get_width() // 2, self.y))
		elif self.end == True:
			txtObject = self.TextObject.render(self.text, True, self.color)
			surface.blit(txtObject, (self.x - txtObject.get_width(), self.y))
		else:
			txtObject = self.TextObject.render(self.text, True, self.color)
			surface.blit(txtObject, (self.x, self.y))

	def changeText(self, newText):
		self.text = newText

class Upgrade:
	def __init__(self, image, cost, x, y, addCps=0.1, multiplier=1.2):
		self.image = image
		self.baseCost = cost
		self.cost = cost
		self.x = x
		self.y = y
		self.bought = 0
		self.addCps = addCps
		self.cps = self.bought*self.addCps
		self.multiplier = multiplier

	def check_click(self, mousex, mousey):
		if (self.x <= mousex <= self.x+300) and (self.y <= mousey <= self.y+75):
			return True
		else:
			return False

	def buy(self):
		self.cost = self.cost*self.multiplier
		self.bought += 1
		self.cps = self.bought * self.addCps
		return self.bought

	def getCps(self):
		return float(self.cps/FPS*2)

	def render(self, surface):
		surface.blit(self.image, (self.x, self.y))

class Renderer:
	def __init__(self, surface, items=[]):
		self.items = items
		self.surface = surface
	def append(self, item):
		self.items.append(item)
	def render(self):
		for item in self.items:
			item.render(self.surface)

class Stepper:
	def __init__(self, items=[]):
		self.items = items
	def append(self, item):	
		self.items.append(item)
	def calculate(self):
		total = 0
		for item in self.items:
			if item != []:
				total += item.getCps()

		return total

def main():
	global cookies, cursors, cps

	pygame.init()
	pygame.display.set_caption('Cookie Clicker')

	# CONSTANTS
	UPGRADES      = "UPGRADES"
	GAME          = "GAME"
	# VARIABLES
	DISPLAYSURF   = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
	FPSCLOCK      = pygame.time.Clock()
	STAGE         = GAME
	Loop          = True
	BGCOLOR       = WHITE
	# GAME RELATED VARIABLES
	cookies = 0
	cursors = 0
	cps = (cursors*1)
	# OBJECTS
	objCookie     = Cookie(pygame.image.load('cookie.png'))
	objCursor     = Upgrade(pygame.image.load('cursor.png'), 15, WINDOWWIDTH-300, 0)
	objGrandma    = Upgrade(pygame.image.load('grandma.png'), 100, WINDOWWIDTH-300, 0+75, addCps=1)
	fontSystem    = Text("Cookies: "+str(cookies), objCookie.x+250, objCookie.y-50, 50, BLACK, middle=True)
	fontCps       = Text("CPS: "+str(cps), objCookie.x+250, objCookie.y-100, 50, BLACK, middle=True)
	fontCursor    = Text("Price: "+str(round(objCursor.cost, 1)), WINDOWWIDTH-300, 75/2, 30, BLACK, end=True)
	fontGrandma   = Text("Price: "+str(round(objGrandma.cost, 1)), WINDOWWIDTH-300, 75+75/2, 30, BLACK, end=True)
	mainRender    = Renderer(DISPLAYSURF)
	mainCalculate = Stepper()

	mainRender.append(objCookie)
	mainRender.append(objGrandma)
	mainRender.append(objCursor)
	mainRender.append(fontSystem)
	mainRender.append(fontCps)
	mainRender.append(fontCursor)
	mainRender.append(fontGrandma)
	
	mainCalculate.append(objCursor)
	mainCalculate.append(objGrandma)

	while True == Loop:
		global cookies, cursors, cps

		mouseClicked = False
		DISPLAYSURF.fill(BGCOLOR)

		for event in pygame.event.get():

			if (event.type == QUIT) or (event.type == KEYUP and event.key == K_ESCAPE):
				pygame.quit()
				exit(0)

			elif event.type == MOUSEMOTION:
				mouseX, mouseY = event.pos

			elif event.type == MOUSEBUTTONUP:
				mouseX, mouseY = event.pos
				mouseClicked = True

		if True == mouseClicked:
			cookieClicked = objCookie.check_click(mouseX, mouseY)
			if cookieClicked == True:
				cookies += 1
				print "Clicked the big cookie!"

			cursorClicked = objCursor.check_click(mouseX, mouseY)
			if cursorClicked == True:
				if cookies >= objCursor.cost:
					cookies -= objCursor.cost
					objCursor.buy()
					fontCursor.changeText("Price: "+str(round(objCursor.cost, 1)))
					fontCps.changeText("CPS: "+str(round(mainCalculate.calculate()*30, 1)))

			grandmaClicked = objGrandma.check_click(mouseX, mouseY)
			if grandmaClicked == True:
				if cookies >= objGrandma.cost:
					cookies -= objGrandma.cost
					objGrandma.buy()
					fontGrandma.changeText("Price: "+str(round(objGrandma.cost, 1)))
					fontCps.changeText("CPS: "+str(round(mainCalculate.calculate()*30, 1)))

		cps = mainCalculate.calculate()
		cookies += cps

		fontSystem.changeText("Cookies: "+str(round(cookies, 1)))

		mainRender.render()

		pygame.display.update()
		FPSCLOCK.tick(FPS)

if __name__ == "__main__":
	main()